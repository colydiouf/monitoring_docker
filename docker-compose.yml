version: "3.3"

services:

  traefik:
    image: ${IMAGE_TRAEFIK}
    container_name: "traefik"
    restart: always
    hostname: "traefik"
    ports:
      - "${TRAEFIK_PORT}:80"
      - "${TRAEFIK_PORT_HTTPS}:443"
      - "${TRAEFIK_PORT_DASHBOARD}:800"
      - "${TRAEFIK_PORT_METRIC}:8082"
    volumes:
      - "/var/run/docker.sock:/var/run/docker.sock:ro"
      - "./users_credentials:/users_credentials:ro"
      - "./acme.json:/acme.json"
      - "./traefik.dynamic.conf/tls.yml:/etc/traefik/dynamic/certs-traefik.yml"
      - "traefik_log:/var/log/traefik/"
      - "/etc/localtime:/etc/localtime:ro"
    networks:
      - front-end
    command:
      - "--api.insecure=true"
      - "--providers.docker=true"
      - "--api.dashboard=true"
      - "--providers.docker.exposedbydefault=false"
      - "--entrypoints.web.address=:80"
      - "--entrypoints.websecure.address=:443"
      - "--entrypoints.web.http.redirections.entryPoint.to=websecure"
      - "--entrypoints.web.http.redirections.entryPoint.scheme=https"
      - "--certificatesResolvers.lets-encr.acme.email=coly.m@cloud-inspire.io"
      - "--certificatesResolvers.lets-encr.acme.storage=acme.json"
      - "--certificatesResolvers.lets-encr.acme.httpChallenge.entryPoint=web"
      - "--providers.docker.watch=true"
      - "--serverstransport.insecureskipverify=true"
      - "--certificatesresolvers.lets-encr.acme.tlschallenge=true"
      - "--metrics.prometheus=true"
      #- "--metrics.prometheus.addEntryPointsLabels=true"
      #- "--metrics.prometheus.addServicesLabels=true"
      #- "--metrics.prometheus.manualrouting=true"
      - "--metrics.prometheus.buckets=0.100000, 0.300000, 1.200000, 5.000000"
      - "--entryPoints.metrics.address=:8082"
      - "--metrics.prometheus.entryPoint=metrics"
      - "--log.filePath=/var/log/traefik/traefik.log"
      - "--log.format=json"
      - "--accesslog=true"
      - "--accesslog.filepath=/var/log/traefik/access.log"  
    labels:
      - "traefik.enable=true"
      - "traefik.http.routers.traefik.tls.certresolver=lets-encr"
      - "traefik.http.routers.traefik.rule=Host(`trafic.$MY_DOMAIN`)"
      - "traefik.http.routers.traefik.tls"
      - "traefik.http.routers.traefik.tls=true"
      - "traefik.http.routers.traefik.entrypoints=websecure"
      - "traefik.docker.network=front-end"
      - "traefik.http.routers.traefik.service=api@internal"

  web:
    image: ${IMAGE_NGINX}
    container_name: "web-nginx"
    restart: always
    volumes:
      - ${WWW_PATH}:/var/www/html
      - ./config/nginx/default.conf:/etc/nginx/conf.d/default.conf
    depends_on:
      - php
    networks:
      - front-end
    labels:
      - "traefik.enable=true"
      - "traefik.http.routers.nginx.entrypoints=web"
      - "traefik.http.routers.nginx.rule=Host(`mynginx.$MY_DOMAIN`)"
      - "traefik.http.routers.nginx.entrypoints=websecure"
      - "traefik.http.routers.nginx.tls.certresolver=lets-encr"
      - "traefik.http.routers.nginx.tls=true"
      - "traefik.docker.network=front-end"
  php:
    image: ${IMAGE_PHP}
    container_name: "web-php"
    restart: always
    expose:
      - '9000'
    environment:
      - MYSQL_USER=${MYSQL_USER}
      - MYSQL_PASSWORD=${MYSQL_PASSWORD}
    volumes:
      - ${WWW_PATH}:/var/www/html
      - ./config/nginx/default.conf:/etc/nginx/conf.d/default.conf
    depends_on:
      - mysql
      - redis
    networks:
      - front-end
      - back-end

  mysql:
    image: ${IMAGE_MYSQL}
    container_name: "web-mysql"
    restart: always
    volumes:
      - ./.data/db:/var/lib/mysql
    environment:
      - MYSQL_ROOT_PASSWORD=${MYSQL_ROOT_PASSWORD}
      - MYSQL_DATABASE=${MYSQL_DATABASE}
      - MYSQL_USER=${MYSQL_USER}
      - MYSQL_PASSWORD=${MYSQL_PASSWORD}
    networks:
      - back-end
    expose:
      - '3306'

  redis:
    image: ${IMAGE_REDIS}
    container_name: "web-redis"
    restart: always
    networks:
      - back-end
    expose:
      - '6379'
    volumes:
      - ./redis-data:/var/lib/redis

  phpmyadmin:
    image: ${IMAGE_PHPMYADMIN}
    container_name: "web-phpmyadmin"
    restart: always
    depends_on:
      - mysql
    expose:
      - '80'
    environment:
      PMA_HOST : ${PMA_HOST}
      PMA_ROOT_PASSWORD : ${PMA_ROOT_PASSWORD}
    networks:
      - front-end
      - back-end
    labels:
      - "traefik.enable=true"
      - "traefik.http.routers.phpmyadmin.entrypoints=web"
      - "traefik.http.routers.phpmyadmin.rule=Host(`phpmyadmin.$MY_DOMAIN`)"
      - "traefik.http.routers.phpmyadmin.entrypoints=websecure"
      - "traefik.http.routers.phpmyadmin.tls.certresolver=lets-encr"
      - "traefik.http.routers.phpmyadmin.tls=true"
      - "traefik.http.services.phpmyadmin.loadbalancer.server.port=80"
      - "traefik.docker.network=front-end"

  grafana:
    image: ${IMAGE_GRAFANA}
    restart: always
    container_name: grafana
    volumes:
      - ./grafana/grafana.ini:/etc/grafana/grafana.ini
      - ./grafana/provisioning/:/etc/grafana/provisioning/

    user: "472"
    expose:
      - '3000'
    depends_on:
      - prometheus
    networks:
      - front-end
      - back-end
    labels:
      - "traefik.enable=true"
      - "traefik.http.routers.grafana.entrypoints=web"
      - "traefik.http.routers.grafana.rule=Host(`grafana.$MY_DOMAIN`)"
      - "traefik.http.routers.grafana.entrypoints=websecure"
      - "traefik.http.routers.grafana.tls.certresolver=lets-encr"
      - "traefik.http.routers.grafana.tls=true"
      - "traefik.http.services.grafana.loadbalancer.server.port=3000"
      - "traefik.docker.network=front-end"
  prometheus:
    image: ${IMAGE_PROMETHEUS}
    container_name: prometheus
    command:
      - '--config.file=/etc/prometheus/prometheus.yml'
      - '--storage.tsdb.path=/prometheus'
      - '--web.console.libraries=/usr/share/prometheus/console_libraries'
      - '--web.console.templates=/usr/share/prometheus/consoles'
    volumes:
      - ./prometheus/:/etc/prometheus/
      - prometheus_data:/prometheus
    restart: always
    expose:
      - '9090'
    networks:
      - front-end
    labels:
      - "traefik.enable=true"
      - "traefik.http.routers.prometheus.entrypoints=web"
      - "traefik.http.routers.prometheus.rule=Host(`prometheus.$MY_DOMAIN`)"
      - "traefik.http.routers.prometheus.entrypoints=websecure"
      - "traefik.http.routers.prometheus.tls.certresolver=lets-encr"
      - "traefik.http.routers.prometheus.tls=true"
      - "traefik.http.services.prometheus.loadbalancer.server.port=9090"
      - "traefik.docker.network=front-end"

  node-exporter:
    image:  ${IMAGE_NODEXPORTER}
    container_name: node-exporter
    user: root
    privileged: true
    command:
      - '--path.procfs=/host/proc'
      - '--path.sysfs=/host/sys'
      - '--collector.filesystem.ignored-mount-points=^/(sys|proc|dev|host|etc)($$|/)'
    restart: unless-stopped
    expose:
      - '9100'
    volumes:
      - /proc:/host/proc:ro
      - /sys:/host/sys:ro
      - /:/rootfs:ro
    networks:
      - front-end
  nginx-exporter:
    image: ${IMAGE_NGINXEXPORTER}
    container_name: nginx_exporter
    expose:
      - "9113"
    command:
      - -nginx.scrape_uri="http://mynginx.$MY_DOMAIN:80/stub_status"
    networks:
      - front-end

  alertmanager:
    image: ${IMAGE_ALERTMANAGER}
    container_name: alertmanager
    command:
      - "--config.file=/etc/alertmanager/config.yaml"
    restart: always
    volumes:
      - ./alertmanager/config.yaml:/etc/alertmanager/config.yaml
    expose:
      - "9093"
    networks:
      - back-end
      - front-end
    labels:
      - "traefik.enable=true"
      - "traefik.http.routers.alertmanager.entrypoints=web"
      - "traefik.http.routers.alertmanager.rule=Host(`alertmanager.$MY_DOMAIN`)"
      - "traefik.http.routers.alertmanager.entrypoints=websecure"
      - "traefik.http.routers.alertmanager.tls.certresolver=lets-encr"
      - "traefik.http.routers.alertmanager.tls=true"
      - "traefik.http.services.alertmanager.loadbalancer.server.port=9093"
      - "traefik.docker.network=front-end"
    
  mysqld_exporter:
    image: ${IMAGE_MYSQLEXPORTER}
    depends_on:
      - prometheus
    environment:
      - DATA_SOURCE_NAME='mysqld-exporter:12qwaszx@(dbhost:3306)/'
    expose:
      - "9104"
    container_name: mysqld_exporter
    restart: always
    networks:
      - front-end
  pushgateway:
    image: ${IMAGE_PUSHGATEWAY}
    volumes:
      - ./pushgateway_data:/data
    command:
      - "--persistence.file=/data/pushgateway.data"
      - "--persistence.interval=1h"
    expose:
      - 9091
    networks:
      - front-end
  cadvisor:
    image: ${IMAGE_CADVISOR}
    container_name: cadvisor
    restart: always
    expose:
      - "8080"
    privileged: true
    volumes:
      - /:/rootfs:ro
      - /var/run:/var/run:rw
      - /sys:/sys:ro
      - /var/lib/docker/:/var/lib/docker:ro
      - /dev/disk/:/dev/disk:ro
    depends_on:
      - redis
    networks:
      - front-end
    labels:
      org.label-schema.group: "monitoring"

  redis-exporter:
    image: ${IMAGE_REDISEXPORTER}
    container_name: redis-exporter
    restart: unless-stopped
    expose:
      - "9121"
    networks:
      - front-end
    labels:
      org.label-schema.group: "monitoring"
  elasticsearch:
    image: ${IMAGE_ELASTICSEARCH}
    container_name: elasticsearch
    environment:
      - node.name=elasticsearch
      - discovery.type=single-node
      - bootstrap.memory_lock=true
      - ES_JAVA_OPTS=-Xms512m -Xmx512m
    ulimits:
      memlock:
        soft: -1
        hard: -1
    volumes:
      - elasticsearch-data:/usr/share/elasticsearch/data
    expose:
      - "9200"
    networks:
      - back-end
    restart: unless-stopped

  kibana:
    image: ${IMAGE_KIBANA}
    hostname: kibana
    container_name: kibana
    expose:
      - "5601"
    environment:
      ELASTICSEARCH_URL: http://elasticsearch:9200
      ELASTICSEARCH_HOSTS: '["http://elasticsearch:9200"]'
      ELASTICSEARCH_USERNAME: 'elastic'
      ELASTICSEARCH_PASSWORD: 'elastic'
      ELASTICSEARCH_PORT: '9200'
      KIBANA_PASSWORD: 'kibana'
      KIBANA_USERNAME: 'kibana'
    volumes:
      - ./kibana/config/kibana.yml:/usr/share/kibana/config/kibana.yml
    networks:
      - back-end
      - front-end
    depends_on:
      - elasticsearch
    restart: unless-stopped
    
    labels:
      - "traefik.enable=true"
      - "traefik.http.routers.kibana.entrypoints=web"
      - "traefik.http.routers.kibana.rule=Host(`kibana.coly.test.cloud-inspire.io`)"
      - "traefik.http.routers.kibana.entrypoints=websecure"
      - "traefik.http.routers.kibana.tls.certresolver=lets-encr"
      - "traefik.http.routers.kibana.tls=true"
      - "traefik.http.services.kibana.loadbalancer.server.port=5601"
      - "traefik.docker.network=front-end"

  logstash:
    image: ${IMAGE_LOGSTASH}
    container_name: logstash
    hostname: logstash
    environment:
      LS_JAVA_OPTS: "-Xmx256m -Xms256m"      
      LOGSTASH_PASSWORD: 'logstash'
      ELASTICSEARCH_PORT: '9200'
    volumes:
      - ./logstash/config/logstash.yml:/usr/share/logstash/config/logstash.yml
      - ./logstash/pipeline/logstash.conf:/usr/share/logstash/pipeline/logstash.conf
#     - ./logstash/pipeline/cars.conf:/usr/share/logstash/pipeline/logstash.conf
    networks:
      - back-end
    expose:
      - "5000"
    depends_on:
      - elasticsearch
    restart: unless-stopped

  filebeat:
    image: ${IMAGE_FILEBEAT}
    container_name: filebeat
    hostname: filebeat
    user: root
    volumes:
      - ./filebeat/filebeat.yml:/usr/share/filebeat/filebeat.yml:ro
#     - /var/lib/docker/containers:/var/lib/docker/containers:ro 
      - /var/lib/docker:/var/lib/docker:ro
      - /var/run/docker.sock:/var/run/docker.sock
      - traefik_log:/var/log/traefik/
      - filebeat_data:/usr/share/filebeat/data
    command: filebeat -e    
    depends_on:
      - elasticsearch
      - kibana  
    networks:
      - back-end
    restart: unless-stopped
    #command: ["--strict.perms=false"]
    
  metricbeat:
    image: ${IMAGE_METRICBEAT}
    container_name: metricbeat
    cap_add:
      - SYS_PTRACE
      - DAC_READ_SEARCH
    user: root
    volumes:
      - /etc/localtime:/etc/localtime:ro
      - ./metricbeat/config/metricbeat.yml:/usr/share/metricbeat/metricbeat.yml:ro
      - /var/run/docker.sock:/var/run/docker.sock:ro
      - /sys/fs/cgroup:/hostfs/sys/fs/cgroup:ro
      - /proc:/hostfs/proc:ro
      - /run/systemd/private:/run/systemd/private
      - /:/hostfs:ro
      - ./metricbeat_data:/usr/share/metricbeat/data
    networks:
      - back-end
    depends_on:
      - elasticsearch
      - kibana  
    restart: always

  heartbeat: 
    image: ${IMAGE_HEARTBEAT}
    container_name: heartbeat
    hostname: heartbeat
    user: root
    volumes:
      - ./heartbeat/config/heartbeat.yml:/usr/share/heartbeat/heartbeat.yml
      - /var/run/docker.sock:/var/run/docker.sock
    depends_on:
      - elasticsearch
      - kibana  
    command: heartbeat -e -strict.perms=false
    networks: 
      - back-end 
    restart: always 
  
  packetbeat:
    image: ${IMAGE_PACKETBEAT}
    hostname: packetbeat
    user: root
    networks:
      - back-end
    volumes:
      - ./packetbeat/config/packetbeat.yml:/usr/share/packetbeat/packetbeat.yml
      - /var/run/docker.sock:/var/run/docker.sock
    environment:
      - ELASTICSEARCH_HOST= '["http://elasticsearch:9200"]'
      - KIBANA_HOST= '["http://elasticsearch:9200"]'
      - ELASTICSEARCH_USERNAME= elastic
      - ELASTICSEARCH_PASSWORD= elastic
    command: ["--strict.perms=false"]
    cap_add:
      - NET_ADMIN 
      - NET_RAW     
    depends_on:
      - elasticsearch
      - kibana 

networks:
  front-end:
    external: true
  back-end:
    external: true

volumes:
  mysql:
    driver: ${VOLUMES_DRIVER}
  prometheus_data: {}
  elasticsearch-data:
  filebeat_data:
  metricbeat_data:
  traefik_log:
  traefik_ssl:
      
        
